import '../App.css';
// import Language from '../components/LanguageComponent';
import {
  useParams
} from "react-router-dom"
import About from './About';

function Detail() {
  let { id } = useParams();

  return (
    <div className="App">
      <header className="App-header">
          <h1>
            Let's Learn Programming Language
          </h1>
          <h1>ID: {id}</h1>
          <About
            id={id}
            name={'safira'}
          />
      </header>
    </div>
  );
}
export default Detail;
